## DOCUMENTO DE VISIÓN Y ALCANCE  

### INTRODUCCIÓN
 
<p>
El presente documento tiene por objeto documentar la visión general del proyecto.
El objetivo es proveer una visión de alto nivel de los elementos que se abarcarán en el sistema, los que no se incluirán, quienes están involucrados la funcionalidad que abarca el sistema, sus necesidades, los objetivos principales del sistema y las restricciones.
</p>

### ENUNCIADO DEL PROBLEMA
 
##### Necesidad
<p>
La empresa tiene la necesidad de contar con un sistema orientado a la gestión de proyectos ya sea para un proyecto de desarrollo de software tanto como para un proyecto de otra índole, profesional o didáctica.
Problemas y Motivación

Se busca el desarrollo de una plataforma fácil de usar y entender tanto para desarrolladores experimentados como para usuarios de índole no profesional. Las plataformas actuales son estrictamente orientadas al desarrollo de software y proyectos empresariales. 
</p>

### VISIÓN

<p>
La meta principal es lograr brindarle al usuario una herramienta fácil de usar para que pueda gestionar su proyecto de la manera mas eficiente y eficáz posible.
</p>

### SOLUCIÓN PROPUESTA
 
##### Objetivos
<p>
El sistema permitirá soluciones para la creación y organización de proyectos mediante “tareas” y “problemas”. Estas funcionalidades pueden ser tan simple como solo asignarlas a miembros del proyecto o con especificaciones más avanzadas como fecha límite de entrega, prioridad, técnica de desarrollo, etc. para proyectos de índole profesional.
</p>

##### Alcance
<p>
El sistema a desarrollar abarcará las siguientes áreas:
* Creacion de proyectos
* Manejo y asignación de problemas
* Manejo y asignación de Tareas

El sistema a desarrollar no incluirá:
* Wiki
* Canal de chat
* Whiteboard
 
##### Stakeholders y Usuarios

###### Stakeholders
* Scrum Master
* Product Owner
* Desarrolladores

###### Usuarios del sistema
* Equipo de desarrollo de una empresa
* Equipo de estudiantes realizando un trabajo grupal
* Equipo de emprendimiento personal buscando organización

### CRONOGRAMA

| Semana | Activiades | Entregables |
| --- | --- | --- |
| 1 (14/10/17) | Setup de los entornos Java/Xtend, Hibernate y AngularJS/API XTRest. Implementación parcial de las funcionalidades de Proyecto y Usuario. | | 
| 2 (21/10/17) | Log in simplificado. Implementación de creación y borrado de Issue y Task. Implementación de vista de creación y muestra de Issue y Task. | |
| 3 (28/10/17) | Agregar miembros a un proyecto. Agregar prioridad, dificultad y usuario asignado a Issue y Task. | |
| 4 (04/11/17) | Agregar fecha límite a Issue y Task. Eliminar un equipo de trabajo. | |
| 5 (11/11/17) | | |
| 6 (18/11/17) | | |
| 7 (25/11/17) | | |
| 8 (02/12/17) | | |
| 9 (09/12/17) | Reunión de presentación de entregables y cierre de proyecto | |

### RIESGOS

Aceptación del usuario final: El uso incorrecto (o uso incompleto) por parte del usuario final llevará al sistema a no proveer información confiable, llevando a una inadecuada atención a los clientes y a una falta de información para la gestión del negocio.

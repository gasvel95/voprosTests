# Retrospectiva

* Establecer escenario

    Tecnica more-less-keep-throw
    
* Reunir datos


 More | Less | Keep | Throw |
-- | -- | -- | --
 | Documentacion | Codigo repetido |Controllers separados | User stories generales 
 | Division de tareas | Decisiones apresuradas | Taiga| Slack |
 |Reuniones||Discord|
 
 * Generar entendimiento
  Se llego a la conclusion que lo que mas hay que adaptar son aspectos relacionados a la buena organizacion y no tanto al aspecto tecnico

* Decidir que hacer

    : Con respecto a la documentacion se decidio comentar las implementaciones de cada metodo
    : Se decidio organizarse de acuerdo al tiempo disponible de cada integrante
    : Se decidio realizar reuniones dia por medio para mantenernos informados
    : Se decidio usar metodos mas genericos
    : Se decidio crear user stories mas especificas
    
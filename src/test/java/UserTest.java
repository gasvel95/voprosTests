import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import tk.vopros.backend.model.Proyecto;
import tk.vopros.backend.model.Usuario;

public class UserTest {
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		Usuario user = new Usuario();
		Proyecto proyect = new Proyecto("Vopros");
		user.nuevoProyecto(proyect);
		assertEquals(proyect.getCreador(), user);
	}
}

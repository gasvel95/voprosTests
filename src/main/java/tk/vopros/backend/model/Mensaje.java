package tk.vopros.backend.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Mensaje {
	
	@Id @GeneratedValue 
	public Long id;
	
	@ManyToOne(fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private Usuario emisor;
	
	@ManyToOne(fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private Usuario receptor;
	
	private String texto;
	
	public Mensaje(){
		
	}
	
	public Mensaje(Usuario emisor, Usuario receptor, String texto){
		this.emisor = emisor;
		this.receptor = receptor;
		this.texto = texto;
	}

	public Usuario getEmisor() {
		return emisor;
	}

	public Usuario getReceptor() {
		return receptor;
	}

	public String getTexto() {
		return texto;
	}
	
	public void setEmisor(Usuario emisor) {
		this.emisor = emisor;
	}

	public void setReceptor(Usuario receptor) {
		this.receptor = receptor;
	}
}


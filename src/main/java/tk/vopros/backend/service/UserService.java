package tk.vopros.backend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tk.vopros.backend.dao.HibernateUserDAO;
import tk.vopros.backend.model.Usuario;

@Service
public class UserService {
	
	@Autowired
	private HibernateUserDAO userDAO;
	public UserService(){
		userDAO = new HibernateUserDAO();
	}
	
	@Transactional
	public List<Usuario> getAll(){
		return userDAO.getAll();
	}
	
	@Transactional
	public void setUser(Usuario user) {
		userDAO.save(user);
	}
	@Transactional
	public Usuario getById(Long id) {
		return userDAO.getById(id);
	}
	
	@Transactional
	public Usuario getByUsername(String username) {
		return userDAO.getByUsername(username);
	}
	
	
	@Transactional
	public void updateUser(Usuario user) {
		userDAO.update(user);
	}
	
	@Transactional
	public Boolean validate(Usuario user){
		return userDAO.validate(user);
	}
	
	@Transactional
	public List<Usuario> search(String nombre){
		return userDAO.searchUsers(nombre);
	}
}